﻿using WarframeBot.Telegram;

namespace WarframeBot;

public static class Program
{
    public static void Main(string[] args)
    {
        var telegram = new TelegramManager();
        Console.ReadLine();
        telegram.StopReceiving();
    }
}