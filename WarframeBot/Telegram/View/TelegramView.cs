﻿using Telegram.Bot.Types.ReplyMarkups;
using WarframeBot.Telegram.Types;

namespace WarframeBot.Telegram.View;

public static class TelegramView
{
    public static readonly InlineKeyboardMarkup InlineKeyboard = new(new[]
    {
        new[]
        {
            InlineKeyboardButton.WithCallbackData(text: "Продают", callbackData: OrderType.Sell),
            InlineKeyboardButton.WithCallbackData(text: "Покупают", callbackData: OrderType.Buy)
        },
        new[]
        {
            InlineKeyboardButton.WithCallbackData(text: "В игре", callbackData: UserStatusType.InGame),
            InlineKeyboardButton.WithCallbackData(text: "В сети", callbackData: UserStatusType.Online),
            InlineKeyboardButton.WithCallbackData(text: "Оффлайн", callbackData: UserStatusType.Offline)
        }
    });
}