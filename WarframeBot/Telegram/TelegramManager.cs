﻿using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using WarframeBot.Telegram.Handlers;
using WarframeBot.Telegram.Handlers.Impl;

namespace WarframeBot.Telegram;

public class Settings // сделать через бд
{
    public string OrderType { get; set; }

    public string UserStatusType { get; set; }
}

public class TelegramManager
{
    private readonly TelegramBotClient _botClient = new("5841933775:AAG1EfebW9hCPDQ-wLE-aWLsbQlEsnTjD1A");
    private readonly CancellationTokenSource _cts = new();
    public static Dictionary<long, Settings> Users = new();

    private readonly ReceiverOptions _receiverOptions = new()
    {
        AllowedUpdates = Array.Empty<UpdateType>()
    };


    public TelegramManager()
    {
        _botClient.StartReceiving(
            updateHandler: HandleUpdateAsync,
            pollingErrorHandler: HandlePollingErrorAsync,
            receiverOptions: _receiverOptions,
            cancellationToken: _cts.Token);
    }

    private async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update,
        CancellationToken cancellationToken)
    {
        IUserUpdateHandler? updateHandler;
        switch (update.Type)
        {
            case UpdateType.Message:
                updateHandler = new MessageHandler();
                await updateHandler.Handle(botClient, update, cancellationToken);
                break;
            case UpdateType.CallbackQuery:
                updateHandler = new CallbackHandler();
                await updateHandler.Handle(botClient, update, cancellationToken);
                break;
            default:
                await SendMessageAsync(botClient, cancellationToken, update.Message!.Chat.Id, "Неизвестная команда");
                break;
        }
    }


    private Task HandlePollingErrorAsync(ITelegramBotClient botClient, Exception exception,
        CancellationToken cancellationToken)
    {
        var errorMessage = exception switch
        {
            ApiRequestException apiRequestException
                => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
            _ => exception.ToString()
        };

        Console.WriteLine(errorMessage);
        return Task.CompletedTask;
    }

    public void StopReceiving()
    {
        _cts.Cancel();
    }


    public static async Task SendMessageAsync(ITelegramBotClient botClient, CancellationToken cancellationToken,
        long chatId,
        string text)
    {
        await botClient.SendTextMessageAsync(
            chatId: chatId,
            text: text,
            cancellationToken: cancellationToken);
    }
}