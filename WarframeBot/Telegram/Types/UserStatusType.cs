﻿namespace WarframeBot.Telegram.Types;

public static class UserStatusType
{
    public static readonly string InGame = "ingame";
    public static readonly string Online = "online";
    public static readonly string Offline = "offline";
    
    public static bool Contains(string element)
    {
        var list = new List<string>
        {
            InGame,
            Online,
            Offline
        };
        return list.Contains(element);
    }
}