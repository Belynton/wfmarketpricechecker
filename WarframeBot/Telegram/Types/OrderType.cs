﻿namespace WarframeBot.Telegram.Types;

public static class OrderType
{
    public static readonly string Sell = "sell";
    public static readonly string Buy = "buy";

    public static bool Contains(string element)
    {
        var list = new List<string>
        {
            Sell,
            Buy
        };
        return list.Contains(element);
    }

}