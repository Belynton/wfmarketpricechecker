﻿using Telegram.Bot;
using Telegram.Bot.Types;
using WarframeBot.Telegram.Types;

namespace WarframeBot.Telegram.Handlers.Impl.Callbacks;

public class UserStatusTypeChain : CallbackChain
{
    public override async Task NextRequest(CallbackQuery callback, ITelegramBotClient botClient,
        CancellationToken cancellationToken)
    {
        var callbackData = callback.Data!;
        var chatId = callback.Message!.Chat.Id;
        if (OrderType.Contains(callbackData))
        {
            TelegramManager.Users[chatId] = new Settings
            {
                UserStatusType = TelegramManager.Users[chatId].UserStatusType,
                OrderType = callbackData
            };
            await TelegramManager.SendMessageAsync(botClient, cancellationToken, chatId,
                $"Тип заказов для поиска изменен на {callbackData}");
        }
        else if (Successor != null)
        {
            await Successor.NextRequest(callback, botClient, cancellationToken);
        }
    }
}