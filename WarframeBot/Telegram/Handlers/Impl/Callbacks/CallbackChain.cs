﻿using Telegram.Bot;
using Telegram.Bot.Types;

namespace WarframeBot.Telegram.Handlers.Impl.Callbacks;

public abstract class CallbackChain
{
    public CallbackChain Successor { get; set; }

    public abstract Task NextRequest(CallbackQuery callback, ITelegramBotClient botClient,
        CancellationToken cancellationToken);
}