﻿using Telegram.Bot;
using Telegram.Bot.Types;
using WarframeBot.Telegram.Types;

namespace WarframeBot.Telegram.Handlers.Impl.Callbacks;

public class OrderTypeChain : CallbackChain
{
    public override async Task NextRequest(CallbackQuery callback, ITelegramBotClient botClient,
        CancellationToken cancellationToken)
    {
        var callbackData = callback.Data!;
        var chatId = callback.Message!.Chat.Id;
        if (UserStatusType.Contains(callbackData))
        {
            TelegramManager.Users[chatId] = new Settings
            {
                UserStatusType = callbackData,
                OrderType = TelegramManager.Users[chatId].OrderType
            };
            await TelegramManager.SendMessageAsync(botClient, cancellationToken, chatId,
                $"Тип статуса пользователя для поиска изменен на {callbackData}");
        }
        else if (Successor != null)
        {
            await Successor.NextRequest(callback, botClient, cancellationToken);
        }
    }
}