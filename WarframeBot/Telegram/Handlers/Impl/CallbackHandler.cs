﻿using Telegram.Bot;
using Telegram.Bot.Types;
using WarframeBot.Telegram.Handlers.Impl.Callbacks;

namespace WarframeBot.Telegram.Handlers.Impl;

public class CallbackHandler : IUserUpdateHandler
{
    public async Task Handle(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        var callbackQuery = update.CallbackQuery!;
        var orderTypeChain = new OrderTypeChain();
        var userStatusTypeChain = new UserStatusTypeChain();
        orderTypeChain.Successor = userStatusTypeChain;
        await orderTypeChain.NextRequest(callbackQuery, botClient, cancellationToken);
    }
}