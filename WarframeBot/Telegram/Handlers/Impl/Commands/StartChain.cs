﻿using Telegram.Bot;
using Telegram.Bot.Types;
using WarframeBot.Telegram.Types;
using WarframeBot.Telegram.View;

namespace WarframeBot.Telegram.Handlers.Impl.Commands;

public class StartChain : CommandChain
{
    public override async Task NextRequest(Message message, ITelegramBotClient botClient,
        CancellationToken cancellationToken)
    {
        var text = message.Text;
        var chatId = message.Chat.Id;
        CreateUserWithDefaultSettingsIfChatDoesNotExist(chatId);

        if (text.Equals("/start"))
        {
            await botClient.SendTextMessageAsync(
                chatId: message.Chat.Id,
                text: "Настройки бота",
                replyMarkup: TelegramView.InlineKeyboard,
                cancellationToken: cancellationToken);
        }
        else if (Successor != null)
        {
            await Successor.NextRequest(message, botClient, cancellationToken);
        }
    }

    private void CreateUserWithDefaultSettingsIfChatDoesNotExist(long chatId)
    {
        if (!TelegramManager.Users.ContainsKey(chatId))
        {
            TelegramManager.Users.Add(chatId, new Settings
            {
                UserStatusType = UserStatusType.InGame,
                OrderType = OrderType.Sell
            });
        }
    }

}