﻿using Telegram.Bot;
using Telegram.Bot.Types;

namespace WarframeBot.Telegram.Handlers.Impl.Commands;

public abstract class CommandChain
{
    public CommandChain Successor { get; set; }

    public abstract Task NextRequest(Message message, ITelegramBotClient botClient,
        CancellationToken cancellationToken);
}