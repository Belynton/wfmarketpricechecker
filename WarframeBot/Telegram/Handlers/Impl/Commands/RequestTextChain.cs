﻿using System.Text;
using Telegram.Bot;
using Telegram.Bot.Types;
using WarframeBot.Telegram.Types;
using WarframeBot.WarframeMarketApi;

namespace WarframeBot.Telegram.Handlers.Impl.Commands;

public class RequestTextChain : CommandChain
{
    private const int OrderNumber = 5;

    public override async Task NextRequest(Message message, ITelegramBotClient botClient,
        CancellationToken cancellationToken)
    {
        var text = message.Text;
        var chatId = message.Chat.Id;
        if (text is null)
        {
            return;
        }
        
        var result = await GetOrders(chatId, FormatItemName(text));
        if (result.Count == 0)
        {
            await TelegramManager.SendMessageAsync(botClient, cancellationToken, chatId,
                $"Предметы с именем {message.Text} не найдены!");
            return;
        }

        await TelegramManager.SendMessageAsync(botClient, cancellationToken, chatId,
            $"Первые {OrderNumber} заказов со статусом пользователя {TelegramManager.Users[chatId].UserStatusType}:\n" +
            FormatListOfOrdersToOutputString(result));
    }

    private string FormatItemName(string itemName) => itemName.Trim().ToLower().Replace(" ", "_");

    private async Task<List<Order>> GetOrders(long chatId, string itemName)
    {
        var request = new Request(OrderNumber);
        if (TelegramManager.Users[chatId].OrderType.Equals(OrderType.Sell))
        {
            return await request.GetSellOrdersByItemNameAndUserStatusAsync(itemName: itemName,
                userStatus: TelegramManager.Users[chatId].UserStatusType);
        }

        return await request.GetBuyOrdersByItemNameAndUserStatusAsync(itemName: itemName,
            userStatus: TelegramManager.Users[chatId].UserStatusType);
    }
    
    private string FormatListOfOrdersToOutputString(List<Order> orders)
    {
        return new StringBuilder()
            .AppendJoin('\n',
                orders.ConvertAll(order => $"Nickname: {order.User.IngameName} | Platinum: {order.Platinum}"))
            .ToString();
    }
}