﻿using Telegram.Bot;
using Telegram.Bot.Types;
using WarframeBot.Telegram.Handlers.Impl.Commands;

namespace WarframeBot.Telegram.Handlers.Impl;

public class MessageHandler : IUserUpdateHandler
{
    public async Task Handle(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        if (update.Message is not { } message)
        {
            return;
        }

        var startChain = new StartChain();
        var requestTextChain = new RequestTextChain();
        startChain.Successor = requestTextChain;
        await startChain.NextRequest(message, botClient, cancellationToken);
    }
}