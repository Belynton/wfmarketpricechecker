﻿using Telegram.Bot;
using Telegram.Bot.Types;

namespace WarframeBot.Telegram.Handlers;

public interface IUserUpdateHandler
{
    Task Handle(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken);
}