﻿using Newtonsoft.Json;

namespace WarframeBot.WarframeMarketApi;

public class Order
{
    [JsonProperty("platinum")] public int Platinum { get; set; }
    [JsonProperty("quantity")] public int Quantity { get; set; }
    [JsonProperty("order_type")] public string OrderType { get; set; }
    [JsonProperty("user")] public User User { get; set; }
    [JsonProperty("platform")] public string Platform { get; set; }
    [JsonProperty("creation_date")] public DateTime CreationDate { get; set; }
    [JsonProperty("last_update")] public DateTime LastUpdate { get; set; }
    [JsonProperty("visible")] public bool Visible { get; set; }
    [JsonProperty("id")] public string Id { get; set; }
    [JsonProperty("region")] public string Region { get; set; }
}

public class Payload
{
    [JsonProperty("orders")] public List<Order> Orders { get; set; }
}

public class Root
{
    [JsonProperty("payload")] public Payload Payload { get; set; }
}

public class User
{
    [JsonProperty("reputation")] public int Reputation { get; set; }
    [JsonProperty("locale")] public string Locale { get; set; }
    [JsonProperty("avatar")] public string Avatar { get; set; }
    [JsonProperty("last_seen")] public DateTime LastSeen { get; set; }
    [JsonProperty("id")] public string Id { get; set; }
    [JsonProperty("region")] public string Region { get; set; }
    [JsonProperty("status")] public string Status { get; set; }
    [JsonProperty("ingame_name")] public string IngameName { get; set; }
}