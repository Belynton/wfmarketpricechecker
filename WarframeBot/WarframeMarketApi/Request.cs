﻿using Newtonsoft.Json;
using WarframeBot.Telegram.Types;

namespace WarframeBot.WarframeMarketApi;

public class Request
{
    private readonly int _ordersNumber;

    public Request(int ordersNumber)
    {
        _ordersNumber = ordersNumber;
    }

    public async Task<List<Order>> GetSellOrdersByItemNameAndUserStatusAsync(string itemName, string userStatus)
    {
        var result = await GetFilteredOrdersByItemNameAsync(itemName);
        return result.Where(order => order.OrderType.Equals(OrderType.Sell) && order.User.Status.Equals(userStatus))
            .OrderBy(order => order.Platinum)
            .Take(_ordersNumber)
            .ToList();
    }

    public async Task<List<Order>> GetBuyOrdersByItemNameAndUserStatusAsync(string itemName, string userStatus)
    {
        var result = await GetFilteredOrdersByItemNameAsync(itemName);
        return result.Where(order => order.OrderType.Equals(OrderType.Buy) && order.User.Status.Equals(userStatus))
            .OrderBy(order => order.Platinum)
            .Reverse()
            .Take(_ordersNumber)
            .ToList();
    }

    private async Task<List<Order>> GetFilteredOrdersByItemNameAsync(string itemName)
    {
        var uri = $"https://api.warframe.market/v1/items/{itemName}/orders";
        var httpClient = new HttpClient();
        var content = await httpClient.GetStringAsync(uri);
        return content.Contains("DOCTYPE")
            ? new List<Order>()
            : JsonConvert.DeserializeObject<Root>(content).Payload.Orders;
    }
}